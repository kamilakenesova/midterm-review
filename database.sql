CREATE TABLE review (
  id int(11) primary key NOT NULL auto_increment,
  review varchar(100) NOT NULL,
  rate int(3) NOT NULL
);